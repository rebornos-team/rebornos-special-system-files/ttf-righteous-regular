# ttf-righteous-regular

Righteous Regular font used by RebornOS logo

https://fonts.google.com/specimen/Righteous

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-special-system-files/ttf-righteous-regular.git
```

